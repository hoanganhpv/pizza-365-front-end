import { useState } from "react";
import { Collapse, Nav, NavItem, NavLink, Navbar, NavbarToggler } from "reactstrap";

const HeaderComponent = () => {
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);

    return (
        <Navbar expand="sm" light className="shadow sticky-top bg-orange">
            {/* Toggle Button for Mobile Device */}
            <NavbarToggler onClick={toggle} />
            <Collapse isOpen={isOpen} navbar>
                <Nav navbar fill className="w-100">
                    <NavItem active>
                        <NavLink href="#top"><b>Trang chủ</b></NavLink>
                    </NavItem>
                    <NavItem active>
                        <NavLink href="#combo">Combo</NavLink>
                    </NavItem>
                    <NavItem active>
                        <NavLink href="#pizza">Loại Pizza</NavLink>
                    </NavItem>
                    <NavItem active>
                        <NavLink href="#order">Gửi đơn hàng</NavLink>
                    </NavItem>
                </Nav>
            </Collapse>
        </Navbar>
    )
}

export default HeaderComponent;