import { Box, Button, Container, Grid, ThemeProvider, createTheme } from "@mui/material";
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import FacebookIcon from '@mui/icons-material/Facebook';
import WhatsAppIcon from '@mui/icons-material/WhatsApp';
import InstagramIcon from '@mui/icons-material/Instagram';
import PinterestIcon from '@mui/icons-material/Pinterest';
import TwitterIcon from '@mui/icons-material/Twitter';
import LinkedInIcon from '@mui/icons-material/LinkedIn';

const theme = createTheme({
    palette: {
        primary: {
            main: '#101010'
        },
    }
});

const scrollToTop = () => {
    window.scrollTo({
        top: 0,
    });
};


const FooterComponent = () => {
    return (
        <Container maxWidth="" className="p-4 bg-orange text-center">
            <Grid container>
                <Grid item xs={12}>
                    <h4 className="mt-3"><b>Footer</b></h4>
                    <ThemeProvider theme={theme}>
                        <Button variant="outlined" onClick={scrollToTop} className="mt-3"><ArrowUpwardIcon /> &nbsp; TOP</Button>
                    </ThemeProvider>
                    <Box className="mt-3">
                        <FacebookIcon /> &nbsp;
                        <WhatsAppIcon /> &nbsp;
                        <InstagramIcon /> &nbsp;
                        <PinterestIcon /> &nbsp;
                        <TwitterIcon /> &nbsp;
                        <LinkedInIcon />
                    </Box>
                    <p className="mt-3"><b>Powered by DEVCAMP</b></p>
                </Grid>
            </Grid>
        </Container>
    )
}

export default FooterComponent;