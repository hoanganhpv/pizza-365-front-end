import { FormControl, Grid, InputLabel, MenuItem, Select, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { drinkSelectAction } from "./actions/drinkSelect.action";

const DrinkSelect = () => {
    const dispatch = useDispatch();

    const [drink, setDrink] = useState('');
    const handleChange = event => {
        setDrink(event.target.value);
        dispatch(drinkSelectAction(event.target.value));
    }

    // Get drink data and load to select
    const [drinkData, setDrinkData] = useState([]);
    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch("http://127.0.0.1:8000/devcamp-pizza365/drinks");
            const drinkData = await response.json();
            setDrinkData(drinkData.data);
        }
        fetchData();
    }, [])

    return (
        <div>
            <Grid container className="pt-5 pb-5 text-orange">
                <Grid item xs={12} className="text-center">
                    <Typography variant="h4"><b className="p-1 border-bottom border-warning">Chọn Đồ Uống</b></Typography>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={12}>
                    <FormControl fullWidth size="small" className="bg-light">
                        <InputLabel>Chọn Đồ Uống</InputLabel>
                        <Select label="Chọn Đồ Uống" value={drink} onChange={handleChange}>
                            {drinkData.map((element, index) => {
                                return <MenuItem key={index} value={element.maNuocUong}>{element.tenNuocUong}</MenuItem>
                            })}
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>
        </div>
    )
}

export default DrinkSelect;