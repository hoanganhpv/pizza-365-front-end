import { DRINK_SELECT } from "./drinkSelect.const"

export const drinkSelectAction = (value) => {
    return {
        type: DRINK_SELECT,
        payload: value
    }
}