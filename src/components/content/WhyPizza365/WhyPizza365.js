import { Grid, Typography } from "@mui/material";

const WhyPizza365 = () => {
    return (
        <div>
            <Grid container className="py-5 text-orange">
                <Grid item xs={12} className="text-center">
                    <Typography variant="h4"><b className="p-1 border-bottom border-warning">Tại sao lại Pizza 365?</b></Typography>
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12} md={3} className="p-4 border border-warning rounded text-dark intro-1">
                    <h4 className="p-2">Đa Dạng</h4>
                    <p className="p-2">Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</p>
                </Grid>
                <Grid item xs={12} md={3} className="p-4 border border-warning rounded text-dark intro-2">
                    <h4 className="p-2">Chất Lượng</h4>
                    <p className="p-2">Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                </Grid>
                <Grid item xs={12} md={3} className="p-4 border border-warning rounded text-dark intro-3">
                    <h4 className="p-2">Hương Vị</h4>
                    <p className="p-2">Đảm bảo hương vị ngon, độc, lạ mà cbanj chỉ có thể trải nghiệm từ Pizza 365.</p>
                </Grid>
                <Grid item xs={12} md={3} className="p-4 border border-warning rounded text-dark intro-4">
                    <h4 className="p-2">Dịch Vụ</h4>
                    <p className="p-2">Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến.</p>
                </Grid>
            </Grid>
        </div>
    )
}

export default WhyPizza365;