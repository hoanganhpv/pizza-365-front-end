import { CLOSE_CONFIRM_MODAL, CLOSE_ORDER_CODE_MODAL, CREATE_ORDER_ERROR, CREATE_ORDER_SUCCESS } from "./modalConfirmOrder.const"

export const closeConfirmModalAction = () => {
    return {
        type: CLOSE_CONFIRM_MODAL
    }
}

export const confirmOrderAction = (orderData) => async (dispatch) => {
    try {
        const fetchConfig = {
            body: JSON.stringify(orderData),
            method: "POST",
            headers: { "Content-Type": "application/json" }
        };

        console.log(JSON.stringify(orderData));

        const response = await fetch("http://127.0.0.1:8000/devcamp-pizza365/orders", fetchConfig);
        const data = await response.json();
        console.log(data.orderData);

        return dispatch({
            type: CREATE_ORDER_SUCCESS,
            payload: data.orderData.orderCode
        })
    }

    catch (error) {
        return dispatch({
            type: CREATE_ORDER_ERROR,
            payload: error
        })
    }
}

export const closeOrderCodeModalAction = () => {
    return {
        type: CLOSE_ORDER_CODE_MODAL
    }
}