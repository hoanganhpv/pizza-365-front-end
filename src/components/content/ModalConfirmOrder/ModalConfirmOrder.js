import { Box, Button, Grid, Modal, TextField, Typography } from "@mui/material";
import { useDispatch, useSelector } from "react-redux"
import { Container } from "reactstrap";
import { closeConfirmModalAction, closeOrderCodeModalAction, confirmOrderAction } from "./actions/modalConfirOrder.action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 550,
    bgcolor: 'background.paper',
    boxShadow: 24,
    p: 2,
    borderRadius: "10px"
};

const ModalConfirmOrder = () => {
    const dispatch = useDispatch();
    const { comboData, pizzaType, drink, formSubmit, discount, confirmModalOpen, orderCodeModalOpen, orderCode } = useSelector(reduxData => reduxData.homepageReducer);

    // handle close confirm order modal
    const closeConfirmModal = () => {
        dispatch(closeConfirmModalAction());
    }
    
    // handle close order code modal
    const closeOrderCodeModal = () => {
        dispatch(closeOrderCodeModalAction());
    }

    const confirmOrderHandler = () => {
        // khai báo đối tượng chữa dữ liệu tạo mới order
        const orderData = {
            kichCo: comboData.pizzaSize,
            duongKinh: comboData.duongKinh,
            suon: comboData.suon,
            salad: comboData.salad,
            loaiPizza: pizzaType,
            idVoucher: formSubmit.voucher,
            idLoaiNuocUong: drink,
            soLuongNuoc: comboData.nuocNgot,
            hoTen: formSubmit.fullname,
            thanhTien: comboData.thanhTien,
            email: formSubmit.email,
            soDienThoai: formSubmit.phone,
            diaChi: formSubmit.address,
            loiNhan: formSubmit.message
        }
        // dispatch action về reducer
        dispatch(confirmOrderAction(orderData));
    }

    return (
        <Container>
            <Modal open={confirmModalOpen} onClose={closeConfirmModal}>
                <Box sx={style}>
                    <Typography textAlign="center" variant="h5" sx={{ mb: 3 }}>CONFIRM ORDER</Typography>

                    <Grid container>
                        <Grid item xs={12} my={1}>
                            <Typography variant="h6">THÔNG TIN ĐƠN HÀNG</Typography>
                        </Grid>
                    </Grid>

                    <Grid container spacing={1}>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Kích Cỡ" value={comboData.pizzaSize} fullWidth margin="dense" />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Đường Kính" value={comboData.duongKinh} fullWidth margin="dense" />
                        </Grid>
                    </Grid>

                    <Grid container spacing={1}>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Loại Pizza" value={pizzaType} fullWidth margin="dense" />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Sườn" value={comboData.suon} fullWidth margin="dense" />
                        </Grid>
                    </Grid>

                    <Grid container spacing={1}>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Loại Nước Uống" value={drink} fullWidth margin="dense" />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Salad" value={comboData.salad} fullWidth margin="dense" />
                        </Grid>
                    </Grid>

                    <Grid container spacing={1}>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Voucher" value={formSubmit.voucher} fullWidth margin="dense" />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Số Lượng Nước" value={comboData.nuocNgot} fullWidth margin="dense" />
                        </Grid>
                    </Grid>

                    <Grid container spacing={1}>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Giảm Giá" value={discount.toLocaleString('en-us')} fullWidth margin="dense" />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField size="small" label="Thành Tiền" value={comboData.thanhTien.toLocaleString('en-us')} fullWidth margin="dense" />
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={12} my={1}>
                            <Typography variant="h6">THÔNG TIN NGƯỜI DÙNG</Typography>
                        </Grid>
                    </Grid>

                    <Grid container>
                        <Grid item xs={12}>
                            <TextField size="small" label="Họ Tên" value={formSubmit.fullname} fullWidth margin="dense" />
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField size="small" label="Email" value={formSubmit.email} fullWidth margin="dense" />
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField size="small" label="Số Điện Thoại" value={formSubmit.phone} fullWidth margin="dense" />
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField size="small" label="Địa Chỉ" value={formSubmit.address} fullWidth margin="dense" />
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12}>
                            <TextField size="small" label="Lời Nhắn" value={formSubmit.message} fullWidth margin="dense" />
                        </Grid>
                    </Grid>

                    <Grid container mt={3} spacing={1}>
                        <Grid item xs={12} sm={6}>
                            <Button variant="contained" color="success" fullWidth onClick={confirmOrderHandler}>Confirm Order</Button>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <Button variant="contained" color="error" fullWidth onClick={closeConfirmModal}>Cancel</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>


            <Modal open={orderCodeModalOpen} onClose={closeOrderCodeModal}>
                <Box sx={style}>
                    <Typography textAlign="center" variant="h5" sx={{ mb: 3 }}><b>ORDER CREATED SUCCESSFUL</b></Typography>

                    <Grid container>
                        <Grid item xs={12} my={1}>
                            <Typography variant="h6">Đơn hàng được tạo thành công</Typography>
                            <Typography variant="h6">Mã đơn hàng của bạn là: <b>{orderCode}</b></Typography>
                        </Grid>
                    </Grid>

                    <Grid container spacing={1}>
                        <Grid item xs={12} textAlign="right">
                            <Button variant="contained" color="success" onClick={closeOrderCodeModal}>OK</Button>
                        </Grid>
                    </Grid>
                </Box>
            </Modal>
        </Container>
    )
}

export default ModalConfirmOrder;