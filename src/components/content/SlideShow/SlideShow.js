import { Grid } from "@mui/material";
import image_1 from "../../../assets/images/1.jpg";
import image_2 from "../../../assets/images/2.jpg";
import image_3 from "../../../assets/images/3.jpg";
import image_4 from "../../../assets/images/4.jpg";
import Slider from "react-slick";


const SlideShow = () => {
    const settings = {
        dots: true,
        infinite: true,
        speed: 1000,
        fade: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        cssEase: "ease-in-out",
        arrows: false
    };

    return (
        <Grid container>
            <Grid item xs={12}>
                <Slider {...settings}>
                    <img src={image_1} className="w-100" alt="Slide-1" />
                    <img src={image_2} className="w-100" alt="Slide-2" />
                    <img src={image_3} className="w-100" alt="Slide-3" />
                    <img src={image_4} className="w-100" alt="Slide-5" />
                </Slider>
            </Grid>
        </Grid>
    )
}

export default SlideShow;