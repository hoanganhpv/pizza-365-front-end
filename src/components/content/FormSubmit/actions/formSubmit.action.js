import { INPUT_ADDRESS_CHANGE, INPUT_EMAIL_CHANGE, INPUT_MESSAGE_CHANGE, INPUT_NAME_CHANGE, INPUT_PHONE_CHANGE, INPUT_VOUCHER_CHANGE } from "./formSubmit.const"

export const nameChangeAction = (value) => {
    return {
        type: INPUT_NAME_CHANGE,
        payload: value
    }
}

export const phoneChangeAction = (value) => {
    return {
        type: INPUT_PHONE_CHANGE,
        payload: value
    }
}

export const emailChangeAction = (value) => {
    return {
        type: INPUT_EMAIL_CHANGE,
        payload: value
    }
}

export const voucherChangeAction = (value) => {
    return {
        type: INPUT_VOUCHER_CHANGE,
        payload: value
    }
}

export const addressChangeAction = (value) => {
    return {
        type: INPUT_ADDRESS_CHANGE,
        payload: value
    }
}

export const messageChangeAction = (value) => {
    return {
        type: INPUT_MESSAGE_CHANGE,
        payload: value
    }
}