import { Grid, TextField, Typography } from "@mui/material";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addressChangeAction, emailChangeAction, messageChangeAction, nameChangeAction, phoneChangeAction, voucherChangeAction } from "./actions/formSubmit.action";

const FormSubmit = () => {
    const dispatch = useDispatch();

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [voucher, setVoucher] = useState("");
    const [message, setMessage] = useState("");

    const onNameChange = (event) => {
        setName(event.target.value);
        dispatch(nameChangeAction(event.target.value));
    }
    const onEmailChange = (event) => {
        setEmail(event.target.value);
        dispatch(emailChangeAction(event.target.value));
    }
    const onPhoneChange = (event) => {
        setPhone(event.target.value);
        dispatch(phoneChangeAction(event.target.value));
    }
    const onAddressChange = (event) => {
        setAddress(event.target.value);
        dispatch(addressChangeAction(event.target.value));
    }
    const onVoucherChange = (event) => {
        setVoucher(event.target.value);
        dispatch(voucherChangeAction(event.target.value));
    }
    const onMessageChange = (event) => {
        setMessage(event.target.value);
        dispatch(messageChangeAction(event.target.value));
    }
 
    return (
        <div>
            <Grid container id="order" className="pt-5 pb-4 text-orange">
                <Grid item xs={12} className="text-center">
                    <Typography variant="h4"><b className="p-1 border-bottom border-warning">Gửi Đơn Hàng</b></Typography >
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12}>
                    <TextField variant="outlined" value={name} onChange={onNameChange} className="bg-light" size="small" fullWidth label="Họ Và Tên" margin="normal" />
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12}>
                    <TextField variant="outlined" value={email} onChange={onEmailChange} className="bg-light" size="small" fullWidth label="Email" margin="normal" />
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12}>
                    <TextField variant="outlined" value={phone} onChange={onPhoneChange} className="bg-light" size="small" fullWidth label="Số Điện Thoại" margin="normal" />
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12}>
                    <TextField variant="outlined" value={address} onChange={onAddressChange} className="bg-light" size="small" fullWidth label="Địa Chỉ" margin="normal" />
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12}>
                    <TextField variant="outlined" value={voucher} onChange={onVoucherChange} className="bg-light" size="small" fullWidth label="Mã Giảm Giá" margin="normal" />
                </Grid>
            </Grid>

            <Grid container>
                <Grid item xs={12}>
                    <TextField variant="outlined" value={message} onChange={onMessageChange} className="bg-light" size="small" fullWidth label="Lời Nhắn" margin="normal" />
                </Grid>
            </Grid>

        </div>
    )
}

export default FormSubmit;