import { Grid, Typography } from "@mui/material";

const Introduction = () => {
    return (
        <Grid container>
            <Grid item sx={{mb: 2}} xs={12} className="text-orange">
                <Typography variant="h3"><b>PIZZA 365</b></Typography>
                <Typography variant="h6"><b><i>Truly Italian !</i></b></Typography>
            </Grid>
        </Grid>
    )
}

export default Introduction;