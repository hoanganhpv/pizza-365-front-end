import Introduction from "./Introduction/Introduction";
import SlideShow from "./SlideShow/SlideShow";
import WhyPizza365 from "./WhyPizza365/WhyPizza365";
import PizzaSize from "./PizzaSize/PizzaSize";
import PizzaType from "./PizzaType/PizzaType";
import DrinkSelect from "./DrinkSelect/DrinkSelect";
import FormSubmit from "./FormSubmit/FormSubmit";
import ButtonSubmit from "./ButtonSubmit/ButtonSubmit";
import { Container } from "@mui/material";
import SnackbarAlert from "./SnackbarAlert/SnackbarAlert";
import ModalConfirmOrder from "./ModalConfirmOrder/ModalConfirmOrder";

const ContentComponent = () => {
    return (
        <Container className="main-page">
            <Introduction />
            <SlideShow />
            <WhyPizza365 />
            <PizzaSize />
            <PizzaType />
            <DrinkSelect />
            <FormSubmit />
            <ButtonSubmit />
            <ModalConfirmOrder />
            <SnackbarAlert />
        </Container>
    )
}

export default ContentComponent;