import { Button, Card, CardContent, CardHeader, Grid, Typography } from "@mui/material";
import { useDispatch } from "react-redux";
import { useState } from "react";
import { btnLargeClickHandlerAction, btnMediumClickHandlerAction, btnSmallClickHandlerAction } from "./actions/pizzaSize.action";

const PizzaSize = () => {
    const dispatch = useDispatch();

    // state set color for selected button
    const [btnSmall, setBtnSmall] = useState("outlined");
    const [btnMedium, setBtnMedium] = useState("outlined");
    const [btnLarge, setBtnLarge] = useState("outlined");

    const btnSmallClickHandler = () => {
        setBtnSmall("contained");
        setBtnMedium("outlined");
        setBtnLarge("outlined");
        dispatch(btnSmallClickHandlerAction());
    }

    const btnMediumClickHandler = () => {
        setBtnSmall("outlined");
        setBtnMedium("contained");
        setBtnLarge("outlined");
        dispatch(btnMediumClickHandlerAction());
    }

    const btnLargeClickHandler = () => {
        setBtnSmall("outlined");
        setBtnMedium("outlined");
        setBtnLarge("contained");
        dispatch(btnLargeClickHandlerAction());
    }

    return (
        <div>
            <Grid container id="combo" className="pt-5 pb-3 text-orange">
                <Grid item xs={12} className="text-center">
                    <Typography variant="h4"><b className="p-1 border-bottom border-warning">Chọn Combo Pizza</b></Typography>
                    <p className="mt-2">Chọn Combo Pizza phù hợp với nhu cầu của bạn</p>
                </Grid>
            </Grid>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={4}>
                    {/* Card Combo Small */}
                    <Card onClick={btnSmallClickHandler} color="light" className="card text-center">
                        <CardHeader title="S (Small)" className="bg-orange" />
                        <CardContent>
                            <Typography>Đường kính: <b>20cm</b></Typography> <hr />
                            <Typography>Sườn nướng: <b>2</b></Typography> <hr />
                            <Typography>Salad: <b>200g</b></Typography> <hr />
                            <Typography>Nước ngọt: <b>2</b></Typography> <hr />
                            <Typography variant="h4"><b>150.000</b></Typography>
                            <Typography><b>VNĐ</b></Typography>
                            <Button sx={{mt: 2}} color="warning" variant={btnSmall} fullWidth><b>Chọn</b></Button>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} sm={6} md={4}>
                    {/* Card Combo Medium */}
                    <Card onClick={btnMediumClickHandler} color="light" className="card text-center">
                        <CardHeader title="M (Medium)" className="bg-warning" />
                        <CardContent>
                            <Typography>Đường kính: <b>25cm</b></Typography> <hr />
                            <Typography>Sườn nướng: <b>4</b></Typography> <hr />
                            <Typography>Salad: <b>300g</b></Typography> <hr />
                            <Typography>Nước ngọt: <b>3</b></Typography> <hr />
                            <Typography variant="h4"><b>200.000</b></Typography>
                            <Typography><b>VNĐ</b></Typography>
                            <Button sx={{mt: 2}} color="warning" variant={btnMedium} fullWidth><b>Chọn</b></Button>
                        </CardContent>
                    </Card>
                </Grid>

                <Grid item xs={12} sm={6} md={4}>
                    {/* Card Combo Large */}
                    <Card onClick={btnLargeClickHandler} color="light" className="card text-center">
                        <CardHeader title="L (Large)" className="bg-orange" />
                        <CardContent>
                            <Typography>Đường kính: <b>30cm</b></Typography> <hr />
                            <Typography>Sườn nướng: <b>8</b></Typography> <hr />
                            <Typography>Salad: <b>500g</b></Typography> <hr />
                            <Typography>Nước ngọt: <b>4</b></Typography> <hr />
                            <Typography variant="h4"><b>250.000</b></Typography>
                            <Typography><b>VNĐ</b></Typography>
                            <Button sx={{mt: 2}} color="warning" variant={btnLarge} fullWidth><b>Chọn</b></Button>
                        </CardContent>         
                    </Card>
                </Grid>
            </Grid>
        </div >
    )
}

export default PizzaSize;