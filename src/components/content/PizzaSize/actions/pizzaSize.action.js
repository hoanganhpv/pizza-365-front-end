import { BTN_LARGE_CLICK, BTN_MEDIUM_CLICK, BTN_SMALL_CLICK } from "./pizzaSize.const"

export const btnSmallClickHandlerAction = () => {
    return {
        type: BTN_SMALL_CLICK,
        payload: {
            pizzaSize: "S",
            duongKinh: 20,
            suon: 2,
            salad: 200,
            nuocNgot: 2,
            thanhTien: 150000
        }
    }
}

export const btnMediumClickHandlerAction = () => {
    return {
        type: BTN_MEDIUM_CLICK,
        payload: {
            pizzaSize: "M",
            duongKinh: 25,
            suon: 4,
            salad: 300,
            nuocNgot: 3,
            thanhTien: 200000
        },
    }
}

export const btnLargeClickHandlerAction = () => {
    return {
        type: BTN_LARGE_CLICK,
        payload: {
            pizzaSize: "L",
            duongKinh: 30,
            suon: 8,
            salad: 500,
            nuocNgot: 4,
            thanhTien: 250000
        },
    }
}