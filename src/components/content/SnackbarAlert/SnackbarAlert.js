import { Alert, Snackbar } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { closeErrorAlertAction } from "./actions/SnackbarAlert.action";

const SnackbarAlert = () => {
    const dispatch = useDispatch();
    const {error} = useSelector(reduxData => reduxData.homepageReducer);

    const handleClose = () => {
        dispatch(closeErrorAlertAction());
    }

    return (
        <Snackbar open={error.isError} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} variant="filled" severity="error" sx={{ width: '100%' }}>
                {error.errorMessage}
            </Alert>
        </Snackbar>
    )
}

export default SnackbarAlert;