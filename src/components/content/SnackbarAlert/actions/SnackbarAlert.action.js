import { CLOSE_ERROR_ALERT } from "./SnackbarAlert.const"

export const closeErrorAlertAction = () => {
    return {
        type: CLOSE_ERROR_ALERT
    }
}