import seafood from "../../../assets/images/seafood.jpg";
import hawaiian from "../../../assets/images/hawaiian.jpg";
import bacon from "../../../assets/images/bacon.jpg";

import { Button, Card, CardContent, CardMedia, Grid, Typography } from "@mui/material";
import { useDispatch } from "react-redux";
import { useState } from "react";
import { btnBaconClickHandlerAction, btnHawaiiClickHandlerAction, btnSeafoodClickHandlerAction } from "./actions/pizzaType.action";

const PizzaType = () => {
    const dispatch = useDispatch();

    // set color for selected button
    const [btnSeafood, setBtnSeafood] = useState("outlined");
    const [btnHawaii, setBtnHawwaii] = useState("outlined");
    const [btnBacon, setBtnBacon] = useState("outlined");

    const btnSeafoodClickHandler = () => {
        setBtnSeafood("contained");
        setBtnHawwaii("outlined");
        setBtnBacon("outlined");
        dispatch(btnSeafoodClickHandlerAction());
    }

    const btnHawaiiClickHandler = () => {
        setBtnSeafood("outlined");
        setBtnHawwaii("contained");
        setBtnBacon("outlined");
        dispatch(btnHawaiiClickHandlerAction());
    }

    const btnBaconClickHandler = () => {
        setBtnSeafood("outlined");
        setBtnHawwaii("outlined");
        setBtnBacon("contained");
        dispatch(btnBaconClickHandlerAction());
    }

    return (
        <div>
            <Grid container id="pizza" className="py-5 text-orange">
                <Grid item xs={12} className="text-center">
                    <Typography variant="h4"><b className="p-1 border-bottom border-warning">Chọn Loại Pizza</b></Typography>
                </Grid>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={4}>
                    {/* Card Pizza Seafood */}
                    <Card onClick={btnSeafoodClickHandler} className="card">
                        <CardMedia component="img" image={seafood} alt="seafood"></CardMedia>
                        <CardContent>
                            <h5>OCEAN MANIA</h5>
                            <p>PIZZA HẢI SẢN SỐT MAYYONNAISE</p>
                            <p>Xốt Cà Chua, Phô Mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây.</p>
                            <Button sx={{mt: 2}} variant={btnSeafood} color="warning" fullWidth><b>Chọn</b></Button>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                    {/* Card Pizza Hawaii */}
                    <Card onClick={btnHawaiiClickHandler} className="card">
                        <CardMedia component="img" image={hawaiian} alt="hawaiian"></CardMedia>
                        <CardContent>
                            <h5>HAWAIIAN</h5>
                            <p>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                            <p>Xốt Cà Chua, Phô Mai Mozzarella, Xúc Xích, Thịt Dăm Bông, Thơm.</p>
                            <Button sx={{mt: 2}} variant={btnHawaii} color="warning" fullWidth><b>Chọn</b></Button>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item xs={12} sm={6} md={4}>
                    {/* Card Pizza Bacon */}
                    <Card onClick={btnBaconClickHandler} className="card">
                        <CardMedia component="img" image={bacon} alt="bacon"></CardMedia>
                        <CardContent>
                            <h5>CHEESY CHICKEN BACON</h5>
                            <p>PIZZA GÀ PHÔ MAI HEO XÔNG KHÓI</p>
                            <p>Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.</p>
                            <Button sx={{mt: 2}} variant={btnBacon} color="warning" fullWidth><b>Chọn</b></Button>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        </div>
    )
}

export default PizzaType;