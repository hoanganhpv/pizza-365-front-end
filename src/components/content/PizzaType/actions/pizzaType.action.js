import { BTN_BACON_CLICK, BTN_HAWAII_CLICK, BTN_SEAFOOD_CLICK } from "./pizzaType.const"

export const btnSeafoodClickHandlerAction = () => {
    return {
        type: BTN_SEAFOOD_CLICK,
        payload: "SEAFOOD"
    }
}

export const btnHawaiiClickHandlerAction = () => {
    return {
        type: BTN_HAWAII_CLICK,
        payload: "HAWAII"
    }
}

export const btnBaconClickHandlerAction = () => {
    return {
        type: BTN_BACON_CLICK,
        payload: "BACON"
    }
}