import { BUTTON_SUBMIT_CLICK } from "./buttonSubmit.const"

export const btnSubmitClickAction = (voucherCode) => async (dispatch) => {
    // Nếu có điền voucher:
    if (voucherCode) {
        const response = await fetch("http://127.0.0.1:8000/devcamp-pizza365/voucher_detail/" + voucherCode);
        const voucherObj = await response.json();
        let discountPercent = 0;
        // Nếu voucher hợp lệ
        if (voucherObj.data) {
            discountPercent = voucherObj.data.phanTramGiamGia;
        }
        // return thông tin dispatch về reducer
        return dispatch({
            type: BUTTON_SUBMIT_CLICK,
            payload: discountPercent
        })
    }
    else return dispatch({
        type: BUTTON_SUBMIT_CLICK,
        payload: 0
    })
}