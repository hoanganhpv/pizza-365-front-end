import { Button } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { btnSubmitClickAction } from "./actions/buttonSubmit.action";

const ButtonSubmit = () => {
    const dispatch = useDispatch();
    const { formSubmit } = useSelector(reduxData => reduxData.homepageReducer);

    const btnSubmitClickHandler = () => {
        const voucherCode = formSubmit.voucher;
        dispatch(btnSubmitClickAction(voucherCode));
    }

    return (
        <Button sx={{ marginTop: "10px" }} onClick={btnSubmitClickHandler} variant="contained" color="warning" fullWidth><b>Gửi</b></Button>
    )
}

export default ButtonSubmit;