import { combineReducers } from "redux";
import homepageReducer from "./hompage.reducer";

const rootReducer = combineReducers({
    homepageReducer,
});

export default rootReducer;