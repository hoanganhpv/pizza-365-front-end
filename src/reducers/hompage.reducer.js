import { BUTTON_SUBMIT_CLICK } from "../components/content/ButtonSubmit/actions/buttonSubmit.const";
import { DRINK_SELECT } from "../components/content/DrinkSelect/actions/drinkSelect.const";
import { INPUT_ADDRESS_CHANGE, INPUT_EMAIL_CHANGE, INPUT_MESSAGE_CHANGE, INPUT_NAME_CHANGE, INPUT_PHONE_CHANGE, INPUT_VOUCHER_CHANGE } from "../components/content/FormSubmit/actions/formSubmit.const";
import { CLOSE_CONFIRM_MODAL, CLOSE_ORDER_CODE_MODAL, CREATE_ORDER_ERROR, CREATE_ORDER_SUCCESS } from "../components/content/ModalConfirmOrder/actions/modalConfirmOrder.const";
import { BTN_LARGE_CLICK, BTN_MEDIUM_CLICK, BTN_SMALL_CLICK } from "../components/content/PizzaSize/actions/pizzaSize.const";
import { BTN_BACON_CLICK, BTN_HAWAII_CLICK, BTN_SEAFOOD_CLICK } from "../components/content/PizzaType/actions/pizzaType.const";
import { CLOSE_ERROR_ALERT } from "../components/content/SnackbarAlert/actions/SnackbarAlert.const";

const initialState = {
    // pizza size state
    comboData: {
        pizzaSize: "",
        duongKinh: 0,
        suon: 0,
        salad: 0,
        nuocNgot: 0,
        thanhTien: 0,
    },
    // pizza type state
    pizzaType: "",
    // drink state
    drink: "",
    // form submit state
    formSubmit: {
        fullname: "",
        email: "",
        phone: "",
        address: "",
        voucher: "",
        message: "",
    },
    // error handler state
    error: {
        isError: false,
        errorMessage: ""
    },
    // modal confirm state:
    confirmModalOpen: false,
    discount: 0,
    orderCodeModalOpen: false,
    orderCode: ""
}

const homepageReducer = (state = initialState, action) => {
    switch (action.type) {
        // ======== PIZZA COMBO REDUCER ========
        case BTN_SMALL_CLICK:
            state.comboData = action.payload;
            break;

        case BTN_MEDIUM_CLICK:
            state.comboData = action.payload;
            break;

        case BTN_LARGE_CLICK:
            state.comboData = action.payload;
            break;

        // ======== PIZZA TYPE REDUCER ========

        case BTN_SEAFOOD_CLICK:
            state.pizzaType = action.payload;
            break;

        case BTN_HAWAII_CLICK:
            state.pizzaType = action.payload;
            break;

        case BTN_BACON_CLICK:
            state.pizzaType = action.payload;
            break;

        // ======== DRINK REDUCER ========

        case DRINK_SELECT:
            state.drink = action.payload;
            break;

        // ======== FORM SUBMIT REDUCER ========

        case INPUT_NAME_CHANGE:
            state.formSubmit.fullname = action.payload;
            break;

        case INPUT_EMAIL_CHANGE:
            state.formSubmit.email = action.payload;
            break;

        case INPUT_PHONE_CHANGE:
            state.formSubmit.phone = action.payload;
            break;

        case INPUT_ADDRESS_CHANGE:
            state.formSubmit.address = action.payload;
            break;

        case INPUT_VOUCHER_CHANGE:
            state.formSubmit.voucher = action.payload;
            break;

        case INPUT_MESSAGE_CHANGE:
            state.formSubmit.message = action.payload;
            break;

        case CLOSE_ERROR_ALERT:
            state.error.isError = false;
            break;

        // ======== BUTTON SUBMIT REDUCER ========

        case BUTTON_SUBMIT_CLICK:
            if (state.comboData.pizzaSize === "") {
                state.error.isError = true;
                state.error.errorMessage = "Vui lòng chọn Kích cỡ Pizza";
                break;
            }
            else if (state.pizzaType === "") {
                state.error.isError = true;
                state.error.errorMessage = "Vui lòng chọn Loại Pizza";
                break;
            }
            else if (state.drink === "") {
                state.error.isError = true;
                state.error.errorMessage = "Vui lòng chọn Nước Uống";
                break;
            }
            else if (state.formSubmit.fullname === "") {
                state.error.isError = true;
                state.error.errorMessage = "Vui lòng nhập Họ và tên";
                break;
            }
            else if (state.formSubmit.email === "") {
                state.error.isError = true;
                state.error.errorMessage = "Vui lòng nhập Email";
                break;
            }
            else if (state.formSubmit.phone === "") {
                state.error.isError = true;
                state.error.errorMessage = "Vui lòng nhập Số điện thoại";
                break;
            }
            else if (state.formSubmit.address === "") {
                state.error.isError = true;
                state.error.errorMessage = "Vui lòng nhập Địa chỉ";
                break;
            }
            // open modal when all data valid
            state.discount = state.comboData.thanhTien * action.payload / 100;
            state.confirmModalOpen = true;
        break;

        case CLOSE_CONFIRM_MODAL:
            state.confirmModalOpen = false;
            break;

        case CREATE_ORDER_SUCCESS:
            state.orderCode = action.payload;
            // close modal confirm
            state.confirmModalOpen = false;
            // open modal order code
            state.orderCodeModalOpen = true;
            break;

        case CREATE_ORDER_ERROR:
            break

        case CLOSE_ORDER_CODE_MODAL:
            state.orderCodeModalOpen = false;
            break;

        default:
            break;
    }

    return { ...state };
}

export default homepageReducer;